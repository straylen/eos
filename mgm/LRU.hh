// ----------------------------------------------------------------------
// File: LRU.hh
// Author: Andreas-Joachim Peters - CERN
// ----------------------------------------------------------------------

/************************************************************************
 * EOS - the CERN Disk Storage System                                   *
 * Copyright (C) 2011 CERN/Switzerland                                  *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 ************************************************************************/

#ifndef __EOSMGM_LRU__HH__
#define __EOSMGM_LRU__HH__

#include "mgm/Namespace.hh"
#include "common/Mapping.hh"
#include "common/AssistedThread.hh"
#include "namespace/interface/IContainerMD.hh"
#include "XrdOuc/XrdOucErrInfo.hh"
#include <sys/types.h>
#include <memory>

namespace qclient {
  class QClient;
}

EOSMGMNAMESPACE_BEGIN

/**
 * @file   LRU.hh
 *
 * @brief  This class implements an LRU engine to apply policies based on atime
 *
 */

class LRU
{
private:
  AssistedThread mThread; ///< thread id of the LRU thread
  eos::common::VirtualIdentity mRootVid;//< we operate with the root vid
  XrdOucErrInfo mError; //< XRootD error object

public:

  //----------------------------------------------------------------------------
  //! Simple struct describing LRU options
  //----------------------------------------------------------------------------
  struct Options {
    bool enabled;                  //< Is LRU even enabled?
    std::chrono::seconds interval; //< Run LRU every this many seconds.
  };

  //----------------------------------------------------------------------------
  //! Parse an "sys.lru.expire.match" policy
  //! Return true if parsing succeeded, false otherwise
  //----------------------------------------------------------------------------
  static bool parseExpireMatchPolicy(const std::string &policy,
    std::map<std::string, time_t> &matchAgeMap);

  //----------------------------------------------------------------------------
  //! Retrieve current LRU configuration options
  //----------------------------------------------------------------------------
  Options getOptions();

  //----------------------------------------------------------------------------
  //! Retrieve "lru.interval" configuration option as string, or empty if
  //! cannot be found. Assumes gFsView.ViewMutex is at-least readlocked.
  //----------------------------------------------------------------------------
  std::string getLRUIntervalConfig() const;

  //----------------------------------------------------------------------------
  //! Constructor. To run the LRU thread, call Start
  //----------------------------------------------------------------------------
  LRU();

  //----------------------------------------------------------------------------
  //! Start the LRU thread
  //----------------------------------------------------------------------------
  void Start();

  //----------------------------------------------------------------------------
  //! Stop the LRU thread
  //----------------------------------------------------------------------------
  void Stop();

  //----------------------------------------------------------------------------
  //! Destructor - stop the background thread, if running
  //----------------------------------------------------------------------------
  ~LRU();

  /* expire by age if empty
   */
  void AgeExpireEmpty(const char* dir, const std::string& policy);

  /* expire by age
   */
  void AgeExpire(const char* dir, const std::string& policy);

  /* expire by volume
   */
  void CacheExpire(const char* dir, std::string& low, std::string& high);

  /* convert by match
   */
  void ConvertMatch(const char* dir, eos::IContainerMD::XAttrMap& map);

  static const char* gLRUPolicyPrefix;

  struct lru_entry {
    // compare operator to use struct in a map
    bool operator< (lru_entry const& lhs) const
    {
      if (lhs.getCTime() == getCTime()) {
        return (getPath() < lhs.getPath());
      }

      return getCTime() < lhs.getCTime();
    }
    std::string path;
    time_t ctime;
    unsigned long long size;

    // ctime getter
    time_t getCTime() const
    {
      return ctime;
    }

    // path getter
    std::string getPath() const
    {
      return path;
    }
  } ;

  // entry in an lru queue having path name,mtime,size
  typedef struct lru_entry lru_entry_t;

private:
  //----------------------------------------------------------------------------
  // LRU method doing the actual policy scrubbing
  //
  // This thread loops in regular intervals over all directories which have
  // a LRU policy attribute set (sys.lru.*) and applies the defined policy.
  //----------------------------------------------------------------------------
  void backgroundThread(ThreadAssistant& assistant) noexcept;

  //----------------------------------------------------------------------------
  // Process the given directory, apply all policies
  //----------------------------------------------------------------------------
  void processDirectory(const std::string &dir, size_t contentSize,
    eos::IContainerMD::XAttrMap &map);

  //----------------------------------------------------------------------------
  // Perform a single LRU cycle, in-memory namespace
  //----------------------------------------------------------------------------
  void performCycleInMem(ThreadAssistant& assistant) noexcept;

  //----------------------------------------------------------------------------
  // Perform a single LRU cycle, QDB namespace
  //----------------------------------------------------------------------------
  void performCycleQDB(ThreadAssistant& assistant) noexcept;

  //----------------------------------------------------------------------------
  // Internal qclient object
  //----------------------------------------------------------------------------
  std::unique_ptr<qclient::QClient> mQcl;

};

EOSMGMNAMESPACE_END

#endif
